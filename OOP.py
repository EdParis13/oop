class Perro:

    especie = 'mamifero'

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
        
    def descrpcion(self):
        return "{} tiene {} anos de edad".format(self.nombre,self.edad)

    def hablar(self, sonido):
        return "{} dice {}".format(self.nombre, sonido)

class Rottweiler(Perro):
    def correr(self, velocidad):
        return "{} corre a {}". format(self.nombre, velocidad)

lassie = Perro("lassie", 5)
print(lassie.descrpcion())
print(lassie.hablar("Woof Woof"))
roco = Rottweiler("Roco", 10)
print(roco.correr(50))
print(roco.hablar("Que lo que eh"))